API - Application Programming Interface

Purpose:
	Hides the server benaeath an interface layer
		- Hidden Complexity
		- Access Security

REST - Representational State Transfer PROTOCOL
	- is a software architectural style that defines a set of constraints to be used for creating Web services(WIKI)
	-a set of rules for communication between a client and a server.

Purpose:
	-Solve the need to separate user interface concerns of the client from data storage concern of the server.
	-Statelessness
		Server does not need to know client state and vice versa.
		Process VIA resources and HTTP method.
	-Standardized communication
		=Decoupled server to understand, process, and respond to client requests without knowing client state.
		=Implemented via resources represented as (URI) Uniform Resource Indetifier endpoints and HTTPS methods.
	ANATOMY OF CLIENT REQ
		= HTTP METHODS(GET(SELECT)-POST(INSERT)-PUT(UPDATE)-DELETE(DELETE))
		= PATH(URL endpoint)
		= HEADER
		= BODY
		= 

		URI
		=resource/resource category/specific
		=ENTER: GET req API
		= API will repsond
========================
>endpoint == URL
> https://official-joke-api.appspot.com/random_joke == endpoint of an API

>REST is stateless == no data about the request is stored

>Each URL is called a <request> while the data sent back to you is called a <response>.

>Anatomy of REST:
1)The endpoint
2)The method
3)The headers
4)The data (or body)
5)Endpoint: Endpoint(or route) is the URL you request for.

===============1)The endpoint
It follows this structure: root-endpoint/?
EX1: root-endpoint of Github’s API is https://api.github.com 

EX2: <endpoint)https://www.xyz.com/tag/javascript/
oot-endpoint == https://www.xyz.com/
path == tag/javascript/

EX3:
/users/:username/repos > Any colons (:) on a path denotes a variable
https://api.github.com/users/pkmr/repos > <pkmr> username

EX4:query parameters.
They always begin with a question mark (?). Each parameter pair is then separated with an ampersand (&), like this:

?query1=value1&query2=value2

==================2)The method
- GET: Get a resource from the server.
- POST: Create a resource to the server.
- PATCH or PUT: Update existing resource on the server.
- DELETE: Delete existing resource from the server.

==================3)The headers
The additional details provided for communication between client and server

==================== 4)The data (or body)
(also called the body or message) contains the info you want to send to the server.


API Parameters
PATH: required to access the resource E.g. /cars, /fruits
Query Parameters: optional filter the list E.g. /cars?type=SUV&year=2010
Body: Resource specific logic. Advance search query. Sometimes it might have both Query and body.
Header: Should contain global or platform-wide data. E.g. API key parameters, encrypted keys for auth, device type information e.g. mobile or desktop or endpoint, device data type e.g. XML or JSON. Use the header to communicate these parameters





d1>index.html & index.js


