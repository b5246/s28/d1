//Javascript Synchronous vs Asynchronous()

//SYNCHRONOUS (stmt executed one at a time)
console.log(`Hello WOrld`);
//conosole.log(`HEllo AGain`);
console.log(`GoodBye`);

//=============================ASYNCHRONOUS 
//(stmt proceed to execute while other codes are running in the bckground)

//FETCH API
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// CHECK STATUS OF REQ
fetch('https://jsonplaceholder.typicode.com/posts').then(response=>console.log(response.status));

// Retrieve conetnt/data from the RESPONSE object
fetch('https://jsonplaceholder.typicode.com/posts').then((response)=>response.json()).then((json)=>console.log(json));

//Create function using "async" and "await" keywords - asynchronous code(waiting for response)
/*
async - makes a function return a Promise

await-  makes a function wait for a Promise
*/
async function fetchData(argument) {
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');
	console.log(result);
	console.log(typeof result);
	let json = await result.json();
	console.log(json);
}

fetchData();

// Get specific post
fetch('https://jsonplaceholder.typicode.com/posts/48').then(response=>response.json()).then(json=>console.log('Get specific post',json));

// Creating a post
fetch('https://jsonplaceholder.typicode.com/posts', {
	//set method of the "request" to "post"
	// DEFAULT method GET
	method: 'POST',

	//sets the header data of the "request" object to be sent to the backend
	headers: {'Content-Type': 'application/json'}, 

	//set content/body data od the "request" object ti be sent to the backend
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello WOrld',
		userID: 1
	})
}).then(response=> response.json()).then(json=>console.log('CREATE DOC USING POST',json));


//Upadte a post using the PUT method <UPDATE part of DOC>
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method:'PUT',
	headers:{'Content-Type': 'application/json'},
	body: JSON.stringify({
		id:1,
		title:'Updated post',
		body:'hello agaian',
		userId:1
	})
}).then(response=>response.json()).then(json=>console.log('UPADTE part of doc Using PUT',json));

//UPDATE a post using the PATCH method <UPDATE WHOLE DOC>
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method:'PATCH',
	headers:{'Content-Type':'application/json'},
	body:JSON.stringify({title:'Corrected Post'})
}).then(response=>response.json()).then(json=>console.log('UPADTE whole doc Using Patch',json));


// Delete a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method:'DELETE'
});

//filter the posts
fetch('https://jsonplaceholder.typicode.com/posts?userId=1').then(response=>response.json()).then(json=>console.log(`FILTERPOST`,json));









